# Ada 2012 SLK-581 Library

The SLK-581 is a fourteen character statistical linkage code used by
Australian Government departments.

## SLK-581 Algorithm

The code is composed of the ordered concatenation of the following
four data elements related to a person:

1. 2nd, 3rd and 5th letters of last name (family name).

2. 2nd and 3rd letters of first (given) name.

3. The date of birth as a character string in the form DDMMYYYY.

4. The sex of the client as:

    * Code "1" for Male
    * Code "2" for Female
    * Code "3" for Other, "intersex" or "indeterminate"
    * Code "9" for Unknown, not stated or inadequately described

### Additional Notes

* Characters not counted are hyphens, apostrophes, blank spaces, or
  any other character which is not a letter of the alphabet, that may
  appear in a name.

* Where the name is not long enough to supply all the required
  letters, fill the remaining places with a 2 to indicate that a
  letter does not exist. This will occur if the first name is less
  than 3 characters and if the last name is less than 5 characters.

* Where a name or part of a name is missing, substitute a 9 to
  indicate that the letter is unknown.

* Use uppercase letters only for SLK.



## Building

Using the included Makefile, you can build the static, static position
independent code (PIC), and the dynamic shared library, simply by
typing:

```bash
$ make
```

To build a dynamic shared library suitable for calling from C, use:

```bash
$ make dynamic_encapsulated
```

You can also build directly with gprbuild, as per the following examples:

```bash
$ gprbuild -f -P slk_581.gpr -XLib_Kind=Dynamic
$ gprbuild -f -P slk_581.gpr -XLib_Kind=Dynamic_Encapsulated
$ gprbuild -f -P slk_581.gpr -XLib_Kind=Static
$ gprbuild -f -P slk_581.gpr -XLib_Kind=Static_PIC
```

## Testing

The easiest way to build and run the test cases is to use the included
Makefile, and type on the command line:

```bash
$ make test
```

Alternatively, in the top-level directory:

```bash
$ cd test
$ gprbuild -P test_slk_581.gpr -XLib_Kind=Static_PIC
$ ./obj/test_slk_581
```


## Licence

LGPL 2.1




