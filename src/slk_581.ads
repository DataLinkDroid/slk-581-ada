--  SLK-581 Library - Generate SLK-581 codes.
--  Copyright (C) 2018 David K. Trudgett
--
--  This library is free software; you can redistribute it and/or
--  modify it under the terms of the GNU Lesser General Public License
--  version 2.1 as published by the Free Software Foundation.
--
--  This library is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--  Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public
--  License along with this library; if not, write to the Free
--  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
--  Boston, MA 02110-1301 USA

with Ada.Strings.Fixed; use Ada.Strings.Fixed;
with Ada.Strings.Bounded; use Ada.Strings.Bounded;
use Ada.Strings;

package Slk_581 is

   package Name_String is new Generic_Bounded_Length(50);
   subtype Name_Str is Name_String.Bounded_String;
   use Name_String;

   package Gender_String is new Generic_Bounded_Length(20);
   subtype Gender_Str is Gender_String.Bounded_String;
   use Gender_String;

   package Date_String is new Generic_Bounded_Length(10);
   subtype Date_Str is  Date_String.Bounded_String;
   use Date_String;

   function "+" (Str : String) return Name_Str is
      (Name_String.To_Bounded_String(Str));

   function "+" (Str : String) return Gender_Str is
      (Gender_String.To_Bounded_String(Str));

   function "+" (Str : String) return Date_Str is
      (Date_String.To_Bounded_String(Str));

   type Sex_Type is (Male, Female, Other, Unknown);
   -- 'Unknown' is used when the person's sex is not stated or otherwise unknown.
   -- 'Other' is used when the sex or gender is stated, but isn't Male or Female.

   type Data_Rec_Type is tagged record
      First_Name : Name_Str;
      Surname    : Name_Str;
      Gender     : Sex_Type;
      Dob        : Date_Str;
   end record;
   -- Used for storing the data elements that go into constructing the
   -- SLK-581 code.


   function Slk_581
     (Given_Name    : in String;
      Family_Name   : in String;
      Date_Of_Birth : in String;
      Sex           : in Sex_type)
     return String
   with Post => Slk_581_Okay (Slk_581'Result) and then
                Slk_581_Date_Okay (Date_Of_Birth, Slk_581'Result);
   -- Return fourteen-character SLK-581 code.
   -- @param Given_Name Person's first name possibly supplemented with middle name.
   -- @param Family_Name Person's surname or family name.
   -- @param Date_Of_Birth Exact or estimated date of birth in YYYY-MM-DD form.
   -- @param Sex One of the values defined in Sex_Type.


   function Slk_581
     (Given_Name    : in String;
      Family_Name   : in String;
      Date_Of_Birth : in String;
      Sex           : in String)
     return String
   with
     Post => Slk_581_Okay (Slk_581'Result) and then
             Slk_581_Date_Okay (Date_Of_Birth, Slk_581'Result);
   -- Return fourteen-character SLK-581 code.
   -- @param Given_Name Person's first name possibly supplemented with middle name.
   -- @param Family_Name Person's surname or family name.
   -- @param Date_Of_Birth Exact or estimated date of birth in YYYY-MM-DD form.
   -- @param Sex Recognised values: "Male", "Female", "Other", "Intersex", "Indeterminate".
   --            Any other value is treated as Unknown.


   function Data_Rec_To_Slk_581
     (Data_Rec : in Data_Rec_type)
     return String
   with
     Post => Slk_581_Okay (Data_Rec_To_Slk_581'Result) and then
             Slk_581_Date_Okay (To_String (Data_Rec.Dob), Data_Rec_To_Slk_581'Result);
   -- Return fourteen-character SLK-581 code.
   -- @param Data_Rec Record containing the required input data elements.


   function Slk_581_Okay
     (Slk_581 : in String)
     return Boolean;
   -- Perform a check of the general form of the SLK-581 to determine
   -- if the code is valid or not.


   function Slk_581_Date_Okay
     (Iso_8601_Date_Of_Birth : in String;
      Slk_581                : in String)
     return Boolean;
   -- Check that the date of birth appears in the correct location in the SLK-581 code.
   -- @param Iso_8601_Date_Of_Birth DOB in YYYY-MM-DD string format.
   -- @param Slk_581 The SLK-581 code to test.

end Slk_581;
