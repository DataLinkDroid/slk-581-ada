--  SLK-581 Library - Generate SLK-581 codes.
--  Copyright (C) 2018 David K. Trudgett
--
--  This library is free software; you can redistribute it and/or
--  modify it under the terms of the GNU Lesser General Public License
--  version 2.1 as published by the Free Software Foundation.
--
--  This library is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--  Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public
--  License along with this library; if not, write to the Free
--  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
--  Boston, MA 02110-1301 USA

with Ada.Characters.Handling;    use Ada.Characters.Handling;
with Ada.Strings.Fixed.Equal_Case_Insensitive;
with Ada.Calendar;

package body Slk_581 is

   function "=" (Left, Right : String) return Boolean is
     (Equal_Case_Insensitive (Left, right));


   -- Remove non-alpha characters from Name.
   function Cleanse_Name (Name : in Name_Str) return Name_Str
   is
      Clean_Name : Name_Str := +"";
   begin
      for Char of To_String (Name) loop
         if Is_Letter (Char) then
            Name_String.Append (Clean_Name, To_Upper (Char));
         end if;
      end loop;
      return Clean_Name;
   end Cleanse_Name;


   function Get_Surname_Letters (Data_Rec : in Data_Rec_Type) return String
   is
      Surname : String := To_String (Cleanse_Name (Trim (Data_Rec.Surname, Side => Both)));
      Surname_Missing : Boolean := Surname'Length = 0;
      Result : String (1 .. 3) := "999";
   begin
      if Surname_Missing or else Surname'Length = 1 then
         null; -- Return "999"
      elsif Surname'Length < 3 then
         Result (1) := Surname (2);
         Result (2 .. 3) := "22";
      elsif Surname'Length < 5 then
         Result (1 .. 2) := Surname (2 .. 3);
         Result (3) := '2';
      else
         Result (1 .. 2) := Surname ( 2 .. 3);
         Result (3) := Surname (5);
      end if;
      return Result;
   end Get_Surname_Letters;


   function Get_First_Name_Letters (Data_Rec : in Data_Rec_Type) return String
   is
      First_Name : String := To_String (Cleanse_Name (Trim (Data_Rec.First_Name, Side => Both)));
      First_Name_Missing : Boolean := First_Name'Length = 0;
      Result : String (1 .. 2) := "99";
   begin
      if First_Name_Missing or else First_Name'Length = 1 then
         null; -- Return "99"
      elsif First_Name'Length < 3 then
         Result (1) := First_Name (2);
         Result (2) := '2';
      else
         Result (1 .. 2) := First_Name ( 2 .. 3);
      end if;
      return Result;
   end Get_First_Name_Letters;


   function Get_Dob (Data_Rec : in Data_Rec_Type) return String
   with
     Pre  => Length(Data_Rec.Dob) = 10 and then
             To_String (Trim (Data_Rec.Dob, Side => Both)) = To_String (Data_Rec.Dob),
     Post => Get_Dob'Result'Length = 8 and then
             (for all Char of Get_Dob'Result => Is_Digit (char))
   is
      Dob : String := To_String (Data_Rec.Dob);
   begin
      return Dob (9 .. 10) & Dob (6 .. 7) & Dob (1 .. 4);
   end Get_Dob;


   function Get_Sex_Code (Data_Rec : in Data_Rec_type) return String is
     (case Data_Rec.Gender is
         when Male => "1",
         when Female => "2",
         when Other => "3",
         when Unknown => "9")
   with Post => Get_Sex_Code'Result = "1" or else
                Get_Sex_Code'Result = "2" or else
                Get_Sex_Code'Result = "3" or else
                Get_Sex_Code'Result = "9";

   --
   -- Slk_581
   --

   function Slk_581
     (Given_Name    : in String;
      Family_Name   : in String;
      Date_Of_Birth : in String;
      Sex           : in Sex_type)
     return String
   is
      Data_Rec : Data_Rec_Type := (First_Name => +Trim (Given_Name, Side => Both),
                                   Surname    => +Trim (Family_Name, Side => Both),
                                   Gender     => Sex,
                                   Dob        => +Trim (Date_Of_Birth, Side => Both));
   begin
      return Data_Rec_To_Slk_581 (Data_Rec);
   end Slk_581;



   --
   -- Slk_581
   --

   function Slk_581
     (Given_Name    : in String;
      Family_Name   : in String;
      Date_Of_Birth : in String;
      Sex           : in String)
     return String
   is
      Sex_Trimmed : String        := Trim (Sex, Side => Both);
      Data_Rec    : Data_Rec_Type := (First_Name     => +Trim (Given_Name, Side => Both),
                                      Surname        => +Trim (Family_Name, Side => Both),
                                      Gender         => (if
                                                           Sex_Trimmed = "male" then Male
                                                         elsif
                                                           Sex_Trimmed = "female" then Female
                                                         elsif
                                                           Sex_Trimmed = "other" or else
                                                           Sex_Trimmed = "intersex" or else
                                                           Sex_Trimmed = "indeterminate" then Other
                                                         else Unknown),
                                      Dob            => +Trim (Date_Of_Birth, Side => Both));
   begin
      return Data_Rec_To_Slk_581 (Data_Rec);
   end Slk_581;


   --
   -- Data_Rec_To_Slk_581
   --

   function Data_Rec_To_Slk_581
     (Data_Rec : in Data_Rec_type)
     return String
   is
   begin
      return
        Get_Surname_Letters (Data_Rec) &
        Get_First_Name_Letters (Data_Rec) &
        Get_Dob (Data_Rec) &
        Get_Sex_Code (Data_Rec);
   end Data_Rec_To_Slk_581;


   --
   -- Regex_Check_Slk_581
   --

   function Slk_581_Okay
     (Slk_581 : in String)
     return Boolean
   is
      Surname_Part    : String renames Slk_581 (Slk_581'First .. Slk_581'First + 2);
      First_Name_Part : String renames Slk_581 (Slk_581'First + 3 .. Slk_581'First + 4);
      Dob_Part        : String renames Slk_581 (Slk_581'First + 5 .. Slk_581'First + 12);
      Sex_Part        : Character renames Slk_581 (Slk_581'Last);
      Day_Str         : String renames Dob_Part (Dob_Part'First  .. Dob_Part'First + 1);
      Month_Str       : String renames Dob_Part (Dob_Part'First + 2 .. Dob_Part'First + 3);
      Year_Str        : String renames  Dob_Part (Dob_Part'First + 4 .. Dob_Part'First + 7);
      Day_Num         : Natural := Integer'Value (Day_Str);
      Month_Num       : Natural := Integer'Value (Month_Str);
      Year_Num        : Natural := Integer'Value (Year_Str);
   begin
      return

        -- Length is good.
        Slk_581'Length = 14 and then

        -- Consist of only upper case alpha or numeric characters.
        (for all Char of Slk_581 =>
           Is_Digit (Char) or else
           (Is_Letter(Char) and then Is_Upper (Char))) and then

        -- Surname part is good.
        ((for all Char of Surname_Part => Is_Letter (Char)) or else
           Surname_Part = "999" or else
           (Is_Letter (Surname_Part (Surname_Part'First)) and then
            Surname_Part (Surname_Part'First + 1 .. Surname_Part'First + 2) = "22") or else
           (Is_Letter (Surname_Part (Surname_Part'First)) and then
              Is_Letter (Surname_Part (Surname_Part'First + 1)) and then
              Surname_Part (Surname_Part'First + 2) = '2')) and then

        -- First name part is good.
        ((for all Char of First_Name_Part => Is_Letter (Char)) or else
           First_Name_Part = "99" or else
           (Is_Letter (First_Name_Part (First_Name_Part'First)) and then
            First_Name_Part (First_Name_Part'First + 1) = '2')) and then

        -- Dob part is good.
        ((for all Char of Dob_Part => Is_Digit (Char)) and then
           Day_Num > 0 and then
           Day_Num < 32 and then
           Month_Num > 0 and then
           Month_Num < 13 and then
           Year_Num > 1899 and then
           Year_Num <= Ada.Calendar.Year (Ada.Calendar.Clock)) and then

        -- Sex part is good.
        (Sex_Part = '1' or else Sex_Part = '2' or else Sex_Part = '3' or else Sex_Part = '9');

   end Slk_581_Okay;


   function Slk_581_Date_Okay
     (Iso_8601_Date_Of_Birth : in String;
      Slk_581                : in String)
     return Boolean
   is
      Dob : String := Trim (Iso_8601_Date_Of_Birth, Side => Left);  -- Remove any leading spaces.
      Slk : String renames Slk_581;
   begin
      return

        -- Year okay.
        Dob (Dob'First .. Dob'First + 3) = Slk (Slk'First + 9 .. Slk'First + 12) and then

        -- Month okay.
        Dob (Dob'First + 5 .. Dob'First + 6) = Slk (Slk'First + 7 .. Slk'First + 8) and then

        -- Day okay.
        Dob (Dob'First + 8 .. Dob'First + 9) = Slk (Slk'First + 5 .. Slk'First + 6);

   end Slk_581_Date_Okay;


end Slk_581;
