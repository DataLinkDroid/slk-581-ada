with Ada.Text_IO; use Ada.Text_IO;
with Slk_581;     use Slk_581;

use Slk_581.Name_String;


procedure Test_Slk_581 is

   type Test_Rec_Type is new Data_Rec_Type with
      record
         Slk : String (1 .. 14);
      end record;

   Test_Array : constant array (Positive range <>) of Test_Rec_Type :=
                  ((First_Name => +"Jane",
                    Surname    => +"Citizen",
                    Gender     => Female,
                    Dob        => +"1963-05-27",
                    Slk        => "ITZAN270519632"),
                   (First_Name => +"Joseph",
                    Surname    => +"Bloggs",
                    Gender     => Male,
                    Dob        => +"1959-12-31",
                    Slk        => "LOGOS311219591"),
                   (First_Name => +"Jane",
                    Surname    => +"Luca",
                    Gender     => Female,
                    Dob        => +"1963-05-27",
                    Slk        => "UC2AN270519632"),
                   (First_Name => +"Jo",
                    Surname    => +"O'Donnell",
                    Gender     => Female,
                    Dob        => +"1963-05-27",
                    Slk        => "DONO2270519632"),
                   (First_Name => +"J",
                    Surname    => +"Bloggs",
                    Gender     => Female,
                    Dob        => +"1963-05-27",
                    Slk        => "LOG99270519632"),
                   (First_Name => +"J",
                    Surname    => +"Blog",
                    Gender     => Female,
                    Dob        => +"1963-05-27",
                    Slk        => "LO299270519632"),
                   (First_Name => +"J",
                    Surname    => +"O",
                    Gender     => Male,
                    Dob        => +"1959-12-31",
                    Slk        => "99999311219591"),
                   (First_Name => +"J",
                    Surname    => +"Blog",
                    Gender     => Unknown,
                    Dob        => +"1967-06-20",
                    Slk        => "LO299200619679"),
                   (First_Name => +"Joseph",
                    Surname    => +"Bloggs",
                    Gender     => Other,
                    Dob        => +"1959-12-31",
                    Slk        => "LOGOS311219593"));

   Failure_P : Boolean := False;

begin
   for Rec of Test_Array loop
      declare
         Name_Width : constant Positive_Count := 20;
         Full_Name  : String := To_String (Rec.First_Name) & " " & To_String (Rec.Surname);
         Success    : Boolean := Rec.Slk = Data_Rec_To_Slk_581 (Rec);
      begin
         Put (Item => Full_Name);
         Set_Col (To => Name_Width);
         Put (": " & Data_Rec_To_Slk_581 (Rec));
         Set_Col (To => Name_Width + 17);
         Put_Line (": " & (if Success then "PASS" else "FAIL"));
         if not Success then
            Failure_P := True;
         end if;
      end;
   end loop;
   New_Line;
   if Failure_P then
      Put_Line ("One or more tests FAILED.");
   else
      Put_Line ("All tests PASSED.");
   end if;
   New_Line;
end Test_Slk_581;
