# Build slk-581 library


all : static static_pic dynamic

static : 
	gprbuild -XLib_Kind=Static -Pslk_581.gpr

static_pic : 
	gprbuild -XLib_Kind=Static_PIC -Pslk_581.gpr

dynamic : 
	gprbuild -f -XLib_Kind=Dynamic -Pslk_581.gpr

dynamic_encapsulated : 
	gprbuild -f -XLib_Kind=Dynamic_Encapsulated -Pslk_581.gpr

buildtest :
	gprbuild -f -XLib_Kind=Static_PIC -P./test/test_slk_581.gpr

test : buildtest
	./test/obj/test_slk_581

clean :
	gprclean -r -XLib_Kind=Static -Pslk_581
	gprclean -r -XLib_Kind=Static -P./test/test_slk_581.gpr
	gprclean -r -XLib_Kind=Static_PIC -Pslk_581
	gprclean -r -XLib_Kind=Static_PIC -P./test/test_slk_581.gpr
	gprclean -r -XLib_Kind=Dynamic -Pslk_581
	gprclean -r -XLib_Kind=Dynamic_Encapsulated -Pslk_581




# Local Variables:
# eval:(ada-parse-prj-file "slk_581.prj")
# eval:(ada-select-prj-file "slk_581.prj")
# End:
